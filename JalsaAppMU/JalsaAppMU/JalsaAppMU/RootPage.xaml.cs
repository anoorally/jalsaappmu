﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JalsaAppMU
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RootPage : TabbedPage
    {
		public RootPage ()
		{
			InitializeComponent ();

            var today = DateTime.Now.Day;
            //var today = 9;

            //Jalsa dates here
            int[] jalsaDate = { 8, 9, 10 };

            for (int i=0; i<jalsaDate.Length; i++) {
                if (today == jalsaDate[i]) {
                    this.SelectedItem = this.Children[i];
                    break;
                }
            }


        }

    }
}