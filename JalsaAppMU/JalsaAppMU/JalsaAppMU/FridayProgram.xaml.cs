﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JalsaAppMU
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FridayProgram : ContentPage
	{
		public FridayProgram ()
		{
			InitializeComponent ();
            var thisDate = 8;


            var Friday = "SESSION 1;13:00 hrs,Jumu’ah followed by  Namaz ‘Asr,,;,Announcements & Tea,,;SESSION 2;(Presided by Sahid Kaudeer: Naib Amir/National Secretary Jaidaad);	15:05 hrs,Léwàé Ahmadiyyat & Dua,,;15:10 hrs,Tilàwat Quran (Ch 31: v 13-20),Hafiz Karamat Ahmad,;,Translation,Zaheeruddin Kaudeer,;15:20 hrs,Nazam (Naw néhàlàné Jamà‘at …),Shukar Elahi Muiz Sookia,;15:25 hrs,\"Prophet Luqman’s message : Seven principles for success\",Muballigh-e-Silsila Shameem Jamal Ahmad Saheb,Central Missionary;15:55 hrs,LIVE TRANSMISSION OF HUZUR(ATBA)’S FRIDAY SERMON,,;17:00 hrs,Announcements & Dinner,,;18:15 hrs,Namaz Maghrib & ‘Isha,,;End of Day 1;";
            var lines = Friday.Split(';');

            var hightlight = -1;
            var addCount = 0;
            List<int> gridArr = new List<int>();
            var prevTime = new DateTime();


            foreach (var line in lines) {
                var vals = line.Split(',');

                if (vals.Length == 1) {

                    StackLayout tempStack;

                    if (vals[0].Trim().ToLower().Contains("session"))
                    {
                        tempStack = new StackLayout() { BackgroundColor = Color.FromRgba(220, 220, 220, 100) };
                    }
                    else
                    {
                        tempStack = new StackLayout();
                    }
                    var lbl = new Label { Text = vals[0].Trim(), FontAttributes = FontAttributes.Bold, FontSize = 18, HorizontalTextAlignment = TextAlignment.Center, Margin = new Thickness(0, 0, 0, 15) };


                    if (vals[0].Trim().ToLower().Contains("session")) tempStack.Children.Add(new BoxView { HeightRequest = 2, BackgroundColor = Color.LightGray });
                    tempStack.Children.Add(lbl);
                    if (vals[0].Trim().ToLower().Contains("session")) tempStack.Children.Add(new BoxView { HeightRequest = 2, BackgroundColor = Color.LightGray });

                    myStack.Children.Add(tempStack);

                }
                else {

                    var tempStack = new StackLayout();

                    var time = new Label { Text = vals[0].Trim()};
                    var program = new Label { Text = vals[1].Trim(), FontAttributes = FontAttributes.Bold };
                    var person = new Label {Text = vals[2].Trim() };
                    var title = new Label {Text = vals[3].Trim(), FontAttributes = FontAttributes.Italic};


                    try
                    {
                        var scheduleDate = DateTime.Parse(vals[0].Replace("hrs", "").Trim(), System.Globalization.CultureInfo.InvariantCulture);
                        var timeNow = DateTime.Now;

                        if (timeNow > scheduleDate)
                        {
                            hightlight++;
                        }
                        prevTime = scheduleDate;
                    }
                    catch (Exception e)
                    {

                        if (DateTime.Now > prevTime)
                        {
                            hightlight++;
                        }

                    }


                    Grid grid = new Grid
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        RowDefinitions =
                                {
                                    new RowDefinition { Height = GridLength.Auto },
                                    new RowDefinition { Height = GridLength.Auto },
                                    new RowDefinition { Height = GridLength.Auto }
                                },
                        ColumnDefinitions =
                                {
                                    new ColumnDefinition { Width = 100 },
                                    new ColumnDefinition { Width = GridLength.Auto },
                                    new ColumnDefinition { Width = GridLength.Auto }
                                }
                    };


                    Grid.SetRowSpan(time, 3);
                    Grid.SetColumnSpan(program, 2);
                    Grid.SetColumnSpan(person, 2);
                    Grid.SetColumnSpan(title, 2);

                    grid.Children.Add(time, 0, 0);
                    grid.Children.Add(program, 1, 0);
                    grid.Children.Add(person, 1, 1);
                    grid.Children.Add(title, 1, 2);

                    //var abs = new AbsoluteLayout();

                    //AbsoluteLayout.SetLayoutBounds(time, new Rectangle(0, 0, 0.25, 150)); AbsoluteLayout.SetLayoutFlags(time, AbsoluteLayoutFlags.WidthProportional);
                    //AbsoluteLayout.SetLayoutBounds(program, new Rectangle(70, 0, 0.75, 25)); AbsoluteLayout.SetLayoutFlags(program, AbsoluteLayoutFlags.SizeProportional);
                    //AbsoluteLayout.SetLayoutBounds(person, new Rectangle(70, 25, 0.75, 25)); AbsoluteLayout.SetLayoutFlags(person, AbsoluteLayoutFlags.SizeProportional);
                    //AbsoluteLayout.SetLayoutBounds(title, new Rectangle(70, 50, 0.75, 25)); AbsoluteLayout.SetLayoutFlags(title, AbsoluteLayoutFlags.SizeProportional);

                    //abs.Children.Add(time);
                    //abs.Children.Add(program);
                    //abs.Children.Add(person);
                    //abs.Children.Add(title);

                    //tempStack.Children.Add(new BoxView { HeightRequest = 0.5, BackgroundColor = Color.LightGray, Opacity = 0.1 });
                    tempStack.Children.Add(grid);
                    //tempStack.Children.Add(new BoxView { HeightRequest = 1, BackgroundColor = Color.LightGray, Opacity = 0.5 });

                    myStack.Children.Add(tempStack);
                    gridArr.Add(addCount);

                }
                addCount++;

            }

            if (DateTime.Now.Day == thisDate)
            {
                try {
                    myStack.Children.ElementAt(gridArr.ElementAt(hightlight)).BackgroundColor = Color.FromRgb(92, 184, 92);
                }
                catch (Exception e) { }
            }

        }
    }
}