﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JalsaAppMU
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SaturdayProgram : ContentPage
	{
		public SaturdayProgram()
		{
			InitializeComponent ();

            var thisDate = 9;

            var Saturday = "SESSION 3;04:25 hrs,Namaz Tahajjud (Darus Salaam Mosque),,;05:15 hrs,Namaz Fajr,,;05:25 hrs,Darsul Quran,Muballigh-e-Silsila Labeed Goolamaully Saheb,Local Missionary;05:40 hrs,Naashta,,;SESSION 4;(Presided by Shams Varsally: Naib Amir/National Secretary Maal);10:15 hrs,Tilàwat Quran (Ch 29: v 46-50),Jamil Taujoo,,Translation,Wassim Joolfoo,;10:25 hrs,Nazam (Khuda ké pàk logon ko …),Parwez Dulymamode,;10:30 hrs,\"Namaz as a means to restrain oneself from indecency and manifest evil\",Mohammad Aniff Muslam,Nazim Qadha Board;11:00 hrs,\" Living a simple life\",Mukhtar-Din Taujoo,National Secretary Umur Amma;11:30 hrs,Nazam (Wo peshwa hamara …),Nasrullah Jamal Ahmad,;11:35 hrs,\"The exemplary family life of the Holy Prophet (saw)\",Mohammad Ameen Jowahir,National Secretary Tarbiyat/Sadr Ansar;12:05 hrs,Announcements & Lunch,,;SESSION 5;(Presided by Muballigh-e-Silsila Kashif Jamal Ahmad Saheb: Local Missionary);13:15 hrs,Namaz Zuhr & ‘Asr,,;13:35 hrs,Tilàwat Quran (Ch 2: v 219-222),Tawheed Joolfoo,,Translation,Issa Nobee,;13:45 hrs,Nazam (Na ham badlé na tum badlé …),Arshad Soodhun,;13:50 hrs,\"Drugs & Alcohol : Physical, Spiritual & Societal Deterioration\",Nassim Bin Shams Taujoo,National Secretary Wasaya/Sadr Khuddam;14:20 hrs,\"Responsible Use of Internet and Social Media\",Muballigh-e-Silsila Noormohammad Toraubally Saheb,Central Missionary;14:50 hrs,Announcements & Tea,,;SESSION 6;(Presided by Maubarakahmad Boodhun: National Secretary Umur Kharijiyya);15:50 hrs,Arrival of guests,,;16:00 hrs,Tilàwat Quran (Ch 2: v 255-258),Raza Bukhuth,,Translation,Zahid Napaul,;16:10 hrs,Nazam (Khalifa hamara dil hai hamara àka …),Kaleem Soodhun,;16:15 hrs,Address by Guests,,;16:40 hrs,Address by Moussa Taujoo Saheb,,Amir Jamà‘at Mauritius;17:00 hrs,Announcements & Dinner,,;18:15 hrs,Namaz Maghrib & ‘Isha,,;End of Day 2";
            var lines = Saturday.Split(';');

            var hightlight = -1;
            var addCount = 0;
            List<int> gridArr = new List<int>();
            var prevTime = new DateTime();

            foreach (var line in lines)
            {
                var vals = line.Split(',');

                if (vals.Length == 1)
                {

                    StackLayout tempStack;

                    if (vals[0].Trim().ToLower().Contains("session")) {
                        tempStack = new StackLayout() { BackgroundColor = Color.FromRgba(220, 220, 220, 100) };
                    } else {
                        tempStack = new StackLayout();
                    }
                    var lbl = new Label { Text = vals[0].Trim(), FontAttributes = FontAttributes.Bold, FontSize = 18, HorizontalTextAlignment = TextAlignment.Center, Margin = new Thickness(0, 0, 0, 15) };


                    if (vals[0].Trim().ToLower().Contains("session")) tempStack.Children.Add(new BoxView { HeightRequest = 2, BackgroundColor = Color.LightGray });
                    tempStack.Children.Add(lbl);
                    if (vals[0].Trim().ToLower().Contains("session")) tempStack.Children.Add(new BoxView { HeightRequest = 2, BackgroundColor = Color.LightGray });

                    myStack.Children.Add(tempStack);

                }
                else
                {

                    var tempStack = new StackLayout();

                    var time = new Label { Text = vals[0].Trim() };
                    var program = new Label { Text = vals[1].Trim(), FontAttributes = FontAttributes.Bold };
                    var person = new Label { Text = vals[2].Trim() };
                    var title = new Label { Text = vals[3].Trim(), FontAttributes = FontAttributes.Italic };

                    try
                    {
                        var scheduleDate = DateTime.Parse(vals[0].Replace("hrs", "").Trim(), System.Globalization.CultureInfo.InvariantCulture);
                        var timeNow = DateTime.Now;

                        if (timeNow > scheduleDate)
                        {
                            hightlight++;
                        }
                        prevTime = scheduleDate;
                    }
                    catch (Exception e)
                    {

                        if (DateTime.Now > prevTime)
                        {
                            hightlight++;
                        }

                    }


                    Grid grid = new Grid
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        RowDefinitions =
                                {
                                    new RowDefinition { Height = GridLength.Auto },
                                    new RowDefinition { Height = GridLength.Auto },
                                    new RowDefinition { Height = GridLength.Auto }
                                },
                        ColumnDefinitions =
                                {
                                    new ColumnDefinition { Width = 100 },
                                    new ColumnDefinition { Width = GridLength.Auto },
                                    new ColumnDefinition { Width = GridLength.Auto }
                                }
                    };


                    Grid.SetRowSpan(time, 3);
                    Grid.SetColumnSpan(program, 2);
                    Grid.SetColumnSpan(person, 2);
                    Grid.SetColumnSpan(title, 2);

                    grid.Children.Add(time, 0, 0);
                    grid.Children.Add(program, 1, 0);
                    grid.Children.Add(person, 1, 1);
                    grid.Children.Add(title, 1, 2);


                    tempStack.Children.Add(grid);
                    //tempStack.Children.Add(new BoxView {HeightRequest=1, BackgroundColor=Color.LightGray, Opacity=0.7});
                    myStack.Children.Add(tempStack);
                    gridArr.Add(addCount);
                    

                }

                addCount++;
            }

            if (DateTime.Now.Day == thisDate)
            {
                try
                {
                    myStack.Children.ElementAt(gridArr.ElementAt(hightlight)).BackgroundColor = Color.FromRgb(92, 184, 92);
                }
                catch (Exception e) { }
            }            
        }

        
    }
}