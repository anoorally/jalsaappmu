﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JalsaAppMU
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SundayProgram : ContentPage
	{
		public SundayProgram()
		{
			InitializeComponent ();
            var thisDate = 10;


            var Sunday = "SESSION 7;04:25 hrs,Namaz Tahajjud (Darus Salaam Mosque),,;05:15 hrs,Namaz Fajr,,;05:25 hrs,Darsul Quran,Muballigh-e-Silsila Ahmad Ali Khudurrun Saheb,Local Missionary;05:40 hrs,Naashta,,;CLOSING SESSION;(Presided by Moussa Taujoo Saheb: Amir Jamà‘at Mauritius);10:00 hrs,Tilàwat Quran (Ch 24: v 55-58),Shawkat Hosany,,Translation,Wassim Shaik Joomun,;10:10 hrs,Nazam (Har taraf fikr ko daura …),Shahed Hoolash,;10:15 hrs,\"Khilàfat: A Living Proof of the Existence of God\",	Maulana Mozaffar Ahmad Soodhun Saheb,Central Missionary;10:45 hrs,\"The example of the Promised Messiah (as) in following the Noble Sunna of the Holy Prophet(saw)\",Maulana Mujeeb Ahmad Saheb,Missionary-in-Charge;11:15 hrs,Award Ceremony,Noor-ul-Hacq Sookia,National Secretary Ta’lim;11:30 hrs,Concluding Address by Moussa Taujoo Saheb,,Amir Jamà‘at Mauritius;12:10 hrs,Lunch,,;13:15 hrs,Namaz Zuhr & ‘Asr,,";
            var lines = Sunday.Split(';');

            var hightlight = -1;
            var addCount = 0;
            List<int> gridArr = new List<int>();
            var prevTime = new DateTime();

            foreach (var line in lines)
            {
                var vals = line.Split(',');

                if (vals.Length == 1)
                {

                    StackLayout tempStack;

                    if (vals[0].Trim().ToLower().Contains("session"))
                    {
                        tempStack = new StackLayout() { BackgroundColor = Color.FromRgba(220, 220, 220, 100) };
                    }
                    else
                    {
                        tempStack = new StackLayout();
                    }
                    var lbl = new Label { Text = vals[0].Trim(), FontAttributes = FontAttributes.Bold, FontSize = 18, HorizontalTextAlignment = TextAlignment.Center, Margin = new Thickness(0, 0, 0, 15) };


                    if (vals[0].Trim().ToLower().Contains("session")) tempStack.Children.Add(new BoxView { HeightRequest = 2, BackgroundColor = Color.LightGray });
                    tempStack.Children.Add(lbl);
                    if (vals[0].Trim().ToLower().Contains("session")) tempStack.Children.Add(new BoxView { HeightRequest = 2, BackgroundColor = Color.LightGray });

                    myStack.Children.Add(tempStack);

                }
                else
                {

                    var tempStack = new StackLayout();

                    var time = new Label { Text = vals[0].Trim() };
                    var program = new Label { Text = vals[1].Trim(), FontAttributes = FontAttributes.Bold };
                    var person = new Label { Text = vals[2].Trim() };
                    var title = new Label { Text = vals[3].Trim(), FontAttributes = FontAttributes.Italic };

                    try
                    {
                        var scheduleDate = DateTime.Parse(vals[0].Replace("hrs", "").Trim(), System.Globalization.CultureInfo.InvariantCulture);
                        var timeNow = DateTime.Now;

                        if (timeNow > scheduleDate)
                        {
                            hightlight++;
                        }
                        prevTime = scheduleDate;
                    }
                    catch (Exception e)
                    {

                        if (DateTime.Now > prevTime)
                        {
                            hightlight++;
                        }

                    }

                    Grid grid = new Grid
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        RowDefinitions =
                                {
                                    new RowDefinition { Height = GridLength.Auto },
                                    new RowDefinition { Height = GridLength.Auto },
                                    new RowDefinition { Height = GridLength.Auto }
                                },
                        ColumnDefinitions =
                                {
                                    new ColumnDefinition { Width = 100 },
                                    new ColumnDefinition { Width = GridLength.Auto },
                                    new ColumnDefinition { Width = GridLength.Auto }
                                }
                    };


                    Grid.SetRowSpan(time, 3);
                    Grid.SetColumnSpan(program, 2);
                    Grid.SetColumnSpan(person, 2);
                    Grid.SetColumnSpan(title, 2);

                    grid.Children.Add(time, 0, 0);
                    grid.Children.Add(program, 1, 0);
                    grid.Children.Add(person, 1, 1);
                    grid.Children.Add(title, 1, 2);


                    tempStack.Children.Add(grid);
                    //tempStack.Children.Add(new BoxView { HeightRequest = 1, BackgroundColor = Color.LightGray, Opacity = 0.7 });
                    myStack.Children.Add(tempStack);
                    gridArr.Add(addCount);


                }
                addCount++;

            }

            if (DateTime.Now.Day == thisDate)
            {
                try
                {
                    myStack.Children.ElementAt(gridArr.ElementAt(hightlight)).BackgroundColor = Color.FromRgb(92, 184, 92);
                }
                catch (Exception e) { }
            }
        }
    }
}